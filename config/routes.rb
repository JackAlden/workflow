Rails.application.routes.draw do

  devise_for :users
  root 'home#index'
  
  resources :production_batch_dates, :assays, :assay_batches
  
  resources :assignments do
    resources :corrections
    resources :reassignments
  end
  
end
