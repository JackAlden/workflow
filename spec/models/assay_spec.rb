require 'rails_helper'

RSpec.describe Assay, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:assay)).to be_valid
  end
  
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
  
  it { should have_many(:assay_batches).class_name('AssayBatch')}
  it { should have_many(:assay_batches) }
  it { should have_many(:assay_batches).dependent(true) }
end
