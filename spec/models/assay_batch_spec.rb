require 'rails_helper'

RSpec.describe AssayBatch, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:assay_batch)).to be_valid
  end
  
  it { should validate_presence_of(:accession_total) }
  
  it { should belong_to(:production_batch_date).class_name('ProductionBatchDate') }
  it { should belong_to(:assay).class_name('Assay') }
  it { should belong_to(:production_batch_date) }
  it { should belong_to(:assay) }
  
end
