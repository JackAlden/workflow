require 'rails_helper'

RSpec.describe ProductionBatchDate, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:production_batch_date)).to be_valid
  end
  
  it { should validate_uniqueness_of(:batch_date) }
  it { should validate_presence_of(:batch_date) }
  
  it { should have_many(:assay_batches).class_name('AssayBatch')}
  it { should have_many(:assay_batches) }
  it { should have_many(:assay_batches).dependent(true) }

end
