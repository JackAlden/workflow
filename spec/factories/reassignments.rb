FactoryGirl.define do
  factory :reassignment do
    reassigned_from "MyString"
    reassigned_to "MyString"
  end
end
