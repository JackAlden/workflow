FactoryGirl.define do
  factory :correction do
    made_by "MyString"
    missed_by "MyString"
    error_type "MyString"
    description "MyText"
    corrected false
  end
end
