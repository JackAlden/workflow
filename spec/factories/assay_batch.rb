FactoryGirl.define do
  factory :assay_batch do
    production_batch_date_id 1
    assay_id 1
    accession_total { Faker::Number.between(1, 15000) }
  end
end
