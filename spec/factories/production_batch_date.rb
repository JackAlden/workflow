FactoryGirl.define do
  factory :production_batch_date do
    batch_date { Date.today }
    
    trait :with_assay do
      after(:create) do |production_batch_date|
        production_batch_date.assays << create(:production_batch_date)
      end
    end
  end
end