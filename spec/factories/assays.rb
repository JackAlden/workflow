FactoryGirl.define do
  factory :assay do
    name 'NAT Discriminatory'
    
    trait :with_production_batch_date do
      after(:create) do |assay|
        assay.production_batch_dates << create(:production_batch_date)
      end
    end
  end
end
