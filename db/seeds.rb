# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'date'

n = 0

until n == 100
  batch_date = Date.today - 730 + n
  ProductionBatchDate.create!( batch_date: batch_date )
  n += 1
end

assaysList = ['HIV',
              'HCV',
              'HBsAg',
              'ALT',
              'TP',
              'RPR',
              'SPE',
              'ABO',
              'ABY',
              'TET',
              'HBS Titer',
              'Ultrio',
              'Parvo/HAV',
              'NAT Discriminatory',
              'HCV Supplemental']

assaysList.each do |a|
  Assay.create!(name: a)
end

ProductionBatchDate.all.each do |pbd|
  assaysList.each do |a|
    AssayBatch.create!( production_batch_date_id: pbd.id,
                        assay_id: Assay.find_by(name: a).id,
                        accession_total: Faker::Number.between(1,12000)
                      )
  end
end
