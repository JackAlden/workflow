class CreateCorrections < ActiveRecord::Migration
  def change
    create_table :corrections do |t|
      t.belongs_to :assignment, index: true
      
      t.string :made_by
      t.string :missed_by
      t.string :error_type
      t.text :description
      t.boolean :corrected

      t.timestamps null: false
    end
  end
end
