class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.string    :assignment_type
      t.string    :assignment_identifier
      t.datetime  :pick_up_time
      t.string    :assigned_to
      t.string    :status_of, default: 'Pending'
      t.boolean   :review_complete, default: false
      t.datetime  :review_complete_time
      t.integer   :num_samples
      t.text      :notes
      
      t.timestamps null: false
    end
  end
end
