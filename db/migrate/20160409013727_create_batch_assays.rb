class CreateBatchAssays < ActiveRecord::Migration
  def change
    create_table :batch_assays do |t|
      t.belongs_to :production_batch_date, index: true
      t.belongs_to :assay, index: true
      
      t.integer :accession_total
      
      t.timestamps null: false
    end
  end
end
