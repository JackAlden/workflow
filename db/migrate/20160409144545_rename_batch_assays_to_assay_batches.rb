class RenameBatchAssaysToAssayBatches < ActiveRecord::Migration
  def change
    rename_table :batch_assays, :assay_batches
  end
end
