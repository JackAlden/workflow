class CreateReassignments < ActiveRecord::Migration
  def change
    create_table :reassignments do |t|
      t.belongs_to :assignment, index: true
      
      t.string :reassigned_from
      t.string :reassigned_to

      t.timestamps null: false
    end
  end
end
