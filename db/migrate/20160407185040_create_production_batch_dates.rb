class CreateProductionBatchDates < ActiveRecord::Migration
  def change
    create_table :production_batch_dates do |t|
      t.date :batch_date

      t.timestamps null: false
    end
  end
end
