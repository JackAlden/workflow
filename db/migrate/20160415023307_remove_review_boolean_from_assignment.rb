class RemoveReviewBooleanFromAssignment < ActiveRecord::Migration
  def change
    remove_column :assignments, :review_complete_time
    remove_column :assignments, :review_complete
  end
end
