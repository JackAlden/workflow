class ReassignReviewCompleteColumnToAssignments < ActiveRecord::Migration
  def change
    add_column :assignments, :review_complete, :datetime
  end
end
