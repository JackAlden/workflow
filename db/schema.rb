# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.


ActiveRecord::Schema.define(version: 20160429172250) do
  reassignments

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assay_batches", force: :cascade do |t|
    t.integer  "production_batch_date_id"
    t.integer  "assay_id"
    t.integer  "accession_total"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "assay_batches", ["assay_id"], name: "index_assay_batches_on_assay_id", using: :btree
  add_index "assay_batches", ["production_batch_date_id"], name: "index_assay_batches_on_production_batch_date_id", using: :btree

  create_table "assays", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assignments", force: :cascade do |t|
    t.string   "assignment_type"
    t.string   "assignment_identifier"
    t.datetime "pick_up_time"
    t.string   "assigned_to"
    t.string   "status_of",             default: "Pending"
    t.integer  "num_samples"
    t.text     "notes"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.datetime "review_complete"
  end

  create_table "corrections", force: :cascade do |t|
    t.integer  "assignment_id"
    t.string   "made_by"
    t.string   "missed_by"
    t.string   "error_type"
    t.text     "description"
    t.boolean  "corrected"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "corrections", ["assignment_id"], name: "index_corrections_on_assignment_id", using: :btree

  create_table "production_batch_dates", force: :cascade do |t|
    t.date     "batch_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reassignments", force: :cascade do |t|
    t.integer  "assignment_id"
    t.string   "reassigned_from"
    t.string   "reassigned_to"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "reassignments", ["assignment_id"], name: "index_reassignments_on_assignment_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.string   "initials"
    t.boolean  "admin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
