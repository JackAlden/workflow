class AssayBatchesController < ApplicationController
  include ApplicationHelper
  load_and_authorize_resource
  
  def index
    @q            = AssayBatch.ransack(params[:q])
    @assayBatches = @q.result.includes(:assay, :production_batch_date).paginate(page: params[:page], per_page: 10)
  end

  def show
    @assayBatch  = assay_batch_find
    @assignments = Assignment.where(assignment_type: @assayBatch)
  end

  def new
    @assayBatch    = AssayBatch.new
    @batch_options = batch_date_map
    @assay_options = assay_map
  end

  def edit
    @assayBatch    = assay_batch_find
    @batch_options = batch_date_map
    @assay_options = assay_map
  end

  def create
    @assayBatch    = AssayBatch.new(assay_batch_params)
    @batch_options = batch_date_map
    @assay_options = assay_map
    
    if @assayBatch.save
      redirect_to assay_batches_path
    else
      render 'new'
    end 
  end

  def update
    @assayBatch = assay_batch_find
    
    if @assayBatch.update(assay_batch_params)
      redirect_to assay_batches_path
    else
      render 'edit'
    end
  end

  def destroy
    @assayBatch = assay_batch_find
    @assayBatch.destroy
    
    redirect_to assay_batches_path
  end
  
  private
    def assay_batch_params
      params.require(:assay_batch).permit(:production_batch_date_id, :assay_id, :accession_total)
    end
    
    def assay_batch_find
      AssayBatch.find_by_id(params[:id])
    end
    
    def batch_date_map
      current_batch_nums = ProductionBatchDate.where(batch_date: ((1.days.ago)-2.years..(Date.today + 1)-2.years))
      current_batch_nums.map { |b| [ batch_num(b.batch_date), b.id ] }
    end
    
    def assay_map
      Assay.all.map { |a| [ a.name, a.id ] }
    end
end
