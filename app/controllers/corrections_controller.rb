class CorrectionsController < ApplicationController
  load_and_authorize_resource
  
  before_filter :get_assignment
  
  def index
    @corrections = @assignment.corrections.all
  end

  def show
    @correction = correction_find
  end

  def new
    @correction = @assignment.corrections.new
  end

  def edit
    @correction = correction_find
  end
  
  def create
    @correction = @assignment.corrections.new(correction_params)
    
    if @correction.save
      redirect_to assignment_corrections_path
    else
      render 'new'
    end
  end
  
  def update
    @correction = correction_find
    
    if @correction.update(correction_params)
      redirect_to :back
    else
      render 'edit'
    end
  end
  
  def destroy
    @correction = correction_find
    @correction.destroy
    
    redirect_to assignment_corrections_path
  end
  
  private
    def get_assignment
      @assignment = Assignment.find(params[:assignment_id])
    end
    
    def correction_find
      @assignment.corrections.find(params[:id])
    end
  
    def correction_params
      params.require(:correction).permit(:made_by,
                                         :missed_by,
                                         :error_type,
                                         :description,
                                         :corrected)
    end
end
