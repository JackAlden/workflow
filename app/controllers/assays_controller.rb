class AssaysController < ApplicationController
  load_and_authorize_resource
  
  def index
    @assays = Assay.all
  end

  def show
    @assay        = assay_find
    @q            = AssayBatch.where(assay_id: @assay).ransack(params[:q])
    @assayBatches = @q.result.includes(:production_batch_date).paginate(page: params[:page], per_page: 10)
  end

  def new
    @assay = Assay.new
  end

  def edit
    @assay = assay_find
  end

  def create
    @assay = Assay.new(assay_params)
    
    if @assay.save
      redirect_to assays_path
    else
      render 'new'
    end 
  end

  def update
    @assay = assay_find
    
    if @assay.update(assay_params)
      redirect_to assays_path
    else
      render 'edit'
    end
  end

  def destroy
    @assay = assay_find
    @assay.destroy
    
    redirect_to assays_path
  end
  
  private
    def assay_params
      params.require(:assay).permit(:name)
    end
    
    def assay_find
      Assay.find_by_id(params[:id])
    end
end
