class ReassignmentsController < ApplicationController
  before_filter :get_assignment
  
  def index
    @reassignments = @assignment.reassignments.all
  end

  def show
    @reassignment = reassignment_find
  end

  def new
    @reassignment = @assignment.reassignments.new
  end

  def edit
    @reassignment = reassignment_find
  end
  
  def create
    @reassignment = @assignment.reassignments.new(reassignment_params)
    
    if @reassignment.save
      redirect_to assignment_reassignments_path
    else
      render 'new'
    end
  end
  
  def update
    @reassignment = reassignment_find
    
    if @reassignment.update(reassignment_params)
      redirect_to :back
    else
      render 'edit'
    end
  end
  
  def destroy
    @reassignment = reassignment_find
    @reassignment.destroy
    
    redirect_to assignment_reassignments_path
  end
  
  private
    def get_assignment
      @assignment = Assignment.find(params[:assignment_id])
    end
    
    def reassignment_find
      @assignment.reassignments.find(params[:id])
    end
  
    def reassignment_params
      params.require(:reassignment).permit(:reassigned_from,
                                           :reassigned_to)
    end
end
