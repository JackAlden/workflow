class ProductionBatchDatesController < ApplicationController
  load_and_authorize_resource
  
  def index
    @q       = ProductionBatchDate.ransack(params[:q])
    @batches = @q.result.paginate(page: params[:page], per_page: 10)
  end
  
  def show
    @batch        = batch_find
    @assayBatches = AssayBatch.where(production_batch_date_id: @batch)
  end
  
  def new
    @batch = ProductionBatchDate.new
  end
  
  def edit
    @batch = batch_find
  end
  
  def create
    @batch = ProductionBatchDate.new(batch_params)
    
    if @batch.save
      redirect_to production_batch_dates_path
    else
      render 'new'
    end
  end
  
  def update
    @batch = batch_find
    
    if @batch.update(batch_params)
      redirect_to production_batch_dates_path
    else
      render 'edit'
    end
  end
  
  def destroy
    @batch = batch_find
    @batch.destroy
    
    redirect_to production_batch_dates_path
  end
  
  private
    def batch_params
      params.require(:production_batch_date).permit(:batch_date)
    end
    
    def batch_find
      ProductionBatchDate.find_by_id(params[:id])
    end
end
