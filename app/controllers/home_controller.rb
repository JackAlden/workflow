class HomeController < ApplicationController
  
  def index
    @assayBatches = assay_batches_pending
    @assignments  = Assignment.all
    @corrections  = Correction.all
  end
  
  private
    def assay_batches_pending
      current_batch_nums    = ProductionBatchDate.where(batch_date: ((1.days.ago)-2.years..(Date.today + 1)-2.years))
      AssayBatch.where(production_batch_date_id: current_batch_nums)
    end
end
