class AssignmentsController < ApplicationController
  include ApplicationHelper
  load_and_authorize_resource
  
  helper_method :sort_column, :sort_direction
  
  def index
    @assignment   = Assignment.new
    @assignments  = Assignment.order(sort_column + " " + sort_direction)
    @assayBatches = assay_batch_map.compact
  end
  
  def show
    @assignment    = assignment_find
    @corrections   = Correction.where(assignment_id: @assignment)
    @reassignments = Reassignment.where(assignment_id: @assignment)
  end
  
  def edit
    @assignment   = assignment_find
    @assayBatches = assay_batch_map
  end
  
  def create
    @assignment   = Assignment.new(assignment_params)
    @assignments  = Assignment.all
    @assayBatches = assay_batch_map
    
    if @assignment.save
      redirect_to :back
    else
      render 'index'
    end
  end
  
  def update
    @assignment   = assignment_find
    @assayBatches = assay_batch_map
    
    if @assignment.update(assignment_params)
      if @assignment.status_of == 'Pending'
        @assignment.update(review_complete: nil)
      elsif @assignment.status_of == 'Reassigned' || @assignment.status_of == 'Complete'
        @assignment.update(review_complete: Time.now)
      end
      redirect_to :back  
    else
      render 'edit'
    end
  end
  
  def destroy
    @assignment = assignment_find
    @assignment.destroy
    
    redirect_to :back
  end
  
  private
    def assignment_params
      params.require(:assignment).permit(:assignment_type,
                                         :assignment_identifier,
                                         :pick_up_time,
                                         :pick_up_time_string,
                                         :assigned_to,
                                         :status_of,
                                         :review_complete,
                                         :num_samples,
                                         :notes)
    end
    
    def assignment_find
      Assignment.find_by_id(params[:id])
    end
    
    def assay_batch_map
      current_batch_nums    = ProductionBatchDate.where(batch_date: ((1.days.ago)-2.years..(Date.today + 1)-2.years))
      current_assay_batches = AssayBatch.where(production_batch_date_id: current_batch_nums)
      current_assay_batches.map do |ab|
        [ "#{batch_num(ab.production_batch_date.batch_date)} - #{ab.assay.name}", ab.id ]
      end
    end
    
    def sort_column
      %w[ assignment_type
          assignment_identifier
          pick_up_time
          assigned_to
          status_of
          review_complete].include?(params[:sort]) ? params[:sort] : "pick_up_time"
    end
    
    def sort_direction
     %w[ asc desc ].include?(params[:direction]) ? params[:direction] : "asc"
    end
end
