class ProductionBatchDate < ActiveRecord::Base
  has_many :assay_batches, dependent: :delete_all
  has_many :assays, through: :assay_batches
  
  validates :batch_date, presence: true, uniqueness: true
end
