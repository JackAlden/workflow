class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
    else
      can [:create, :read, :update], [ProductionBatchDate,
                                      Assay,
                                      AssayBatch,
                                      Assignment,
                                      Correction]
      can :destroy, Assignment
    end
  end
end
