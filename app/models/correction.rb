class Correction < ActiveRecord::Base
  belongs_to :assignment
  
  validates :made_by, :error_type, :description, presence: true
end
