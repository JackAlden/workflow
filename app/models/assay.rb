class Assay < ActiveRecord::Base
  has_many :assay_batches, dependent: :delete_all
  has_many :production_batch_dates, through: :assay_batches
  
  validates :name, presence: true, uniqueness: true
end
