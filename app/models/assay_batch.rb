class AssayBatch < ActiveRecord::Base
  belongs_to :production_batch_date
  belongs_to :assay
  
  validates :production_batch_date_id, presence: true
  validates :assay_id, presence: true
  validates :accession_total, presence: true
  validates :assay_id, uniqueness: { scope: :production_batch_date_id }
end
