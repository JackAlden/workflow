class Reassignment < ActiveRecord::Base
  belongs_to :assignment
  
  before_save :set_reassigned_from
  after_save :update_assigned_to
  
  def set_reassigned_from
    self.reassigned_from = self.assignment.assigned_to if self.reassigned_from.blank?
  end
  
  def update_assigned_to
    self.assignment.update_assignee_on_reassign!
  end
end
