class Assignment < ActiveRecord::Base
  has_many :corrections, dependent: :delete_all
  has_many :reassignments, dependent: :delete_all
  
  validates_presence_of :assignment_type, :assignment_identifier, :pick_up_time, :assigned_to
  
  def pick_up_time_string
    pick_up_time.to_s
  end
  
  def pick_up_time_string=(pick_up_time_str)
    self.pick_up_time = Chronic.parse(pick_up_time_str)
  rescue ArgumentError
    @pick_up_time_invalid = true
  end
  
  def review_complete_string
    review_complete.to_s
  end
  
  def review_complete_string=(review_complete_str)
    self.review_complete = Chronic.parse(review_complete_str)
  rescue ArgumentError
    @review_complete_invalid = true
  end
  
  def validate
    errors.add(:pick_up_time, "is invalid") if @pick_up_time_invalid
    errors.add(:review_complete, "is invalid") if @review_complete_invalid
  end
  
  def update_assignee_on_reassign!
    self.update_column(:assigned_to, self.reassignments.last.reassigned_to)
  end
end
