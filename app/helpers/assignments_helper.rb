module AssignmentsHelper
  
  def assignment_batch (assigned)
    batch_num(AssayBatch.find_by_id(assigned.assignment_type).production_batch_date.batch_date)
  end
  
  def assignment_assay (assigned)
    AssayBatch.find_by_id(assigned.assignment_type).assay.name
  end
  
  def assignment_name (assigned)
    "#{assignment_batch(assigned)} - #{assignment_assay(assigned)} #{assigned.assignment_identifier}"
  end
  
  def review_state(date)
    if date
      "Yes"
    else
      "No"
    end
  end
end
