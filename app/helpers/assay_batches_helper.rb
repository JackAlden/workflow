module AssayBatchesHelper
  def assay_batch_name(assay_batch)
    assay_batch.assay.name
  end
  
  def assay_batch_num(assay_batch)
    batch_num(assay_batch.production_batch_date.batch_date)
  end
  
  def num_samples_reviewed(assignments)
    assignments.where(status_of: 'Complete').sum(:num_samples)
  end
  
  def num_assignments_completed(assignments)
    assignments.where(status_of: 'Complete').count
  end
end
