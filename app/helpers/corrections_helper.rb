module CorrectionsHelper
  def correction_assay_batch(correction)
    AssayBatch.find_by_id(correction.assignment.assignment_type)
  end
  
  def correction_batch_num(correction)
    batch_num(correction_assay_batch(correction).production_batch_date.batch_date)
  end
  
  def correction_assay_name(correction)
    correction_assay_batch(correction).assay.name
  end
end
