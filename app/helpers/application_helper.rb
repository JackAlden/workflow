module ApplicationHelper
  def lab_date(date)
    if date.class == Time
      date.localtime.strftime("%d%b%y")
    else
      date.strftime("%d%b%y")
    end
  end
  
  def mil_time(time)
    time.localtime.strftime("%H%M")
  end
  
  def lab_date_at_time(datetime)
    "#{lab_date(datetime)} at #{mil_time(datetime)}"
  end
  
  def datetime_placeholder   
    "Today at #{mil_time(Time.now.localtime)}"
  end
  
  def batch_num(date)
    date.strftime("A%d%m%Y")
  end
  
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {sort: column, direction: direction}, {class: css_class}
  end
end
