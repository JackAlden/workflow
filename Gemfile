source 'https://rubygems.org'


gem 'rails',          '~> 4.2.5.1'
gem 'pg',             '~> 0.15'
gem 'sass-rails',     '~> 5.0'
gem 'uglifier',       '>= 1.3.0'
gem 'coffee-rails',   '~> 4.1.0'

gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder',       '~> 2.0'
gem 'sdoc',           '~> 0.4.0', group: :doc

gem 'bootstrap-sass', '~> 3.2.0.0'

# fix broken jquery
gem 'jquery-turbolinks'

# figaro to setup environment variables
gem 'figaro'

# chronic for datetime parsing
gem 'chronic'

# hublot for datetime humanizing
gem 'hublot',         '~> 0.0.6'

# will_paginate to setup pagination
gem 'will_paginate-bootstrap'

# ransack to implement searches
gem 'ransack'

# devise for authentication
gem 'devise'

# cancancan for authorizations
gem 'cancancan',      '~> 1.10'

group :development, :test do
  gem 'rspec-rails',        '~> 3.0'
  gem 'capybara'
  gem 'factory_girl_rails'
  gem 'faker',              '~> 1.6'
  gem 'shoulda-matchers',   '~> 3.1'
  gem 'byebug'
  
  # mini-profiler to optimize speed
  gem 'rack-mini-profiler'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

group :production do
  gem 'rails_12factor', '~> 0.0.2'
  gem 'puma',           '~> 2.11.1'
end

